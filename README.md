# MCP3002
A simple python script for AD-Converter 'MCP3002' on Raspberry Pi.  

# Prerequisites
- Enable SPI by raspi-config
- Install python-pip, spidev  

# Environments
- Hardware: Raspberry Pi 4B (should be work on other Raspberry Pi Board as well)
- OS: Raspberry Pi OS Lite 32bit (should be work on other Raspberry Pi OS)

# Connections
[MCP3002]-----[RPI]  
CS/SHDN-----GPIO8(pin24)  
CH0-----Output of target. (if you choose CH0)  
CH1-----Output of target. (if you choose CH1)  
Vss-----GND(such as pin25), GND of target.  
Din-----GPIO10(pin19)  
Dout-----GPIO9(pin21)  
CLK-----GPIO11(pin23)  
Vdd/Vref-----3.3V(pin1)

![sample connection](references/mcp3002_and_rpi4.jpg)