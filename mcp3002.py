#!/usr/bin/env python
# channel 0 pin1-GND
import spidev
import time
Vref = 3.298 # Measured voltage of RPI 3.3V pin. Measured by tester.

spi = spidev.SpiDev()
spi.open(0,0) #port 0,cs 0
spi.max_speed_hz = 100000 # 100kHz
try:
    while True:
        adc = spi.xfer2([0x68,0x00]) # Ch0
        #adc = spi.xfer2([0x78,0x00]) # Ch1
        data = ((adc[0] & 3) << 8) | adc[1]
        print (str(Vref*data/1024) + "V")
        #print(str(bin(adc[0])) + " | " + str(bin(adc[1])))
        time.sleep(0.1)
except KeyboardInterrupt:
    print('exit')
    spi.close()
